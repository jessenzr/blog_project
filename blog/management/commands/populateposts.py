from django.core.management.base import BaseCommand

from blog.models import Post


class Command(BaseCommand):
    help = 'populate database'

    def handle(self, *args, **options):
        Post.populate()
        Post.delete_missing()
