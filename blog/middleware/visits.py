from blog.models import Visits
from ipware.ip import get_real_ip
from datetime import datetime


class VisitsMiddleware(object):
    def process_request(self, request):
        v = Visits()
        v.ip = get_real_ip(request)
        if v.ip:
            v.page = request.path
            v.created = datetime.now()
            v.save()

    def process_template_response(self, request, response):
        pass
