import os
from blog.settings import MARKDOWN_EXTENSIONS
from markdown import Markdown
from blog.misc import Empty


def get_markdown(path):
    if not os.path.isfile(path):
        return None

    with open(path, 'r') as md_file:
        md = Markdown(extensions=MARKDOWN_EXTENSIONS)
        html = md.convert(md_file.read())

        # if md file has no meta data no need to go on, and we can
        # return its html
        if not hasattr(md, 'Meta'):
            return html

    post = Empty()
    post.html = html
    meta = md.Meta

    for key in meta.keys():
        if len(meta[key]) > 1:
            setattr(post, key, meta[key])
        else:
            setattr(post, key, meta[key][0])

    return post
