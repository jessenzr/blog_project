import os
from blog_project.settings import BASE_DIR

CONTENT_DIR = 'content'
POSTS_DIR = os.path.join(CONTENT_DIR, 'posts')
MARKDOWN_EXTENSIONS = ['meta',
                       'attr_list',
                       'fenced_code',
                       # 'headerid',
]

SITE_URL = 'blog.jessenazario.com'
PROJECT_ID = 'com.jessenazario.blog'
