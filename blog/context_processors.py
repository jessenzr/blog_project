from blog.settings import PROJECT_ID
from blog_project.settings import DEBUG


def blog(request):
    return {'PROJECT_ID': PROJECT_ID,
            'DEBUG': DEBUG, }
