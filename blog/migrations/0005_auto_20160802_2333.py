# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-08-02 23:33
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_posttags_description'),
    ]

    operations = [
        migrations.RenameField(
            model_name='posttags',
            old_name='description',
            new_name='tag',
        ),
    ]
