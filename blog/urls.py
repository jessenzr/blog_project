from django.conf.urls import url
from blog import views

urlpatterns = [
    url(r'^$', views.index, name='index'),

    url(r'^about/$', views.about, name='about'),
    url(r'^post/(?P<slug>[\w-]+)/$', views.post, name='post'),
    url(r'^tag/(?P<tag>[\w-]+)/$', views.tag, name='tag'),
]
