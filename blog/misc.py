class Empty():
    pass


class Paginate(object):

    def __init__(self, obj, page=1, per_page=5):
        self.obj = obj
        self.page = page
        self.per_page = per_page
        self.total_count = len(obj)

        # self._paginate_obj()

    @property
    def pages(self):
        return int(ceil(self.total_count / float(self.per_page)))

    @property
    def show(self):
        return self.pages > 1

    @property
    def has_prev(self):
        return self.page > 1

    def paginate_obj(self):
        obj_paginated = [self.obj[i:i+self.per_page]
                              for i in range(0, self.total_count, self.per_page)]

        self.obj_paginated = obj_paginated[self.page - 1]
        return self.obj_paginated

    @property
    def has_next(self):
        return self.pages < self.pages

    def iter_pages(self, left_edge=2, left_current=2,
                   right_current=5, right_edge=2):
        last = 0

        for num in range(1, self.pages + 1):
            if num <= left_edge or \
               (num > self.page - left_current - 1 and \
                num < self.page + right_current) or \
               num > self.pages - right_edge:
                if last + 1 != num:
                    yield None
                yield num
                last = num

    @property
    def html(self):
        out = []
        out.append('<ul class="pagination pagination-lg">')
        for num in self.iter_pages():
            active = '' if num != self.page else " class=active"

            if isinstance(num, int):
                out.append('<li%s><a href="?page=%s">%s</a></li>'
                           % (active, num, num))
            else:
                out.append('<li class="disabled"><a>...</a></li>')

        out.append('</ul>')

        return "".join(out)
