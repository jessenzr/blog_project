import os
from django.shortcuts import render
from markdown import markdown
from blog.markdown import get_markdown

from blog.models import Post, PostTags
from blog.settings import CONTENT_DIR, MARKDOWN_EXTENSIONS


def index(request):
    posts = Post.get_all()
    return render(request, 'posts.jhtml', {'posts': posts})


def about(request):
    with open(CONTENT_DIR + '/pages/about.md', 'r') as md_file:
        md = markdown(md_file.read(), extensions=MARKDOWN_EXTENSIONS)

    return render(request, 'index.jhtml',
                  {'content': get_markdown(os.path.join(CONTENT_DIR,
                                                        'pages',
                                                        'about.md'))})

def tag(request, tag):
    posts = PostTags.get_posts(tag)
    return render(request, 'posts.jhtml', {'posts': posts})


def post(request, slug):
    post = Post.get_by_slug(slug)
    return render(request, 'post.jhtml', {'content': post})
