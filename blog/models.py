import os

from django.db import models
from django.utils.text import slugify
from markdown import Markdown
from django.utils import timezone

from blog.settings import MARKDOWN_EXTENSIONS, POSTS_DIR
from blog.markdown import get_markdown


class Post(models.Model):
    slug = models.CharField(unique=True, max_length=100)
    file_path = models.CharField(max_length=60)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)

    _metas = ['summary']

    def get_by_slug(slug):
        post = Post.objects.get(slug=slug)
        if post is None:
            return None

        return Post.process_post(post)

    def get_md(path):
        if not os.path.isfile(path):
            return None, None

        with open(path, 'r') as md_file:
            md = Markdown(extensions=MARKDOWN_EXTENSIONS)
            html = md.convert(md_file.read())

            if hasattr(md, 'Meta'):
                return md.Meta, html

            return None, None

    def process_post(post):
        with open(post.file_path, 'r') as md_file:
            md = Markdown(extensions=MARKDOWN_EXTENSIONS)
            post.html = md.convert(md_file.read())

        post.title = md.Meta['title'][0]
        post.tags = PostTags.get_tags_for_post(post)

        return post
        post = get_markdown(post.file_path)
        if not hasattr(post, 'slug'):
            post.slug = slugify(post.title)

        return post

    def get_all(**kwargs):
        posts = []
        if kwargs:
            post_list = Post.objects.filter(kwargs)
        else:
            post_list = Post.objects.all()
        post_list = Post.objects.all().order_by('-date')
        for post in post_list:
            post = Post.process_post(post)
            if post:
                posts.append(post)

        return posts

    def populate():
        for path, folder, files in os.walk(POSTS_DIR):
            for f in files:
                file_path = os.path.join(path, f)
                meta, html = Post.get_md(file_path)

                if not meta:
                    continue

                if 'slug' not in meta:
                    meta['slug'] = [slugify(meta['title'])]

                slug = meta['slug'][0]
                post = Post.objects.get_or_create(slug=slug)[0]
                post.slug = slug
                post.file_path = file_path
                post.date = meta['date'][0]
                post.save()
                Post._save_tags(post, meta)

    def _save_tags(post, meta):
        PostTags.objects.filter(post=post).delete()

        if 'tags' not in meta:
            return
        tags_list = meta['tags'][0].split(' ')
        for tag in tags_list:
            tag_row = PostTags()
            tag_row.post = post
            tag_row.tag = tag
            tag_row.save()


    def delete_missing():
        for post in Post.objects.all():
            if not os.path.isfile(post.file_path):
                post.delete()


class PostTags(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    tag = models.CharField(max_length=40)

    class Meta:
        unique_together = ('post', 'tag',)

    def get_tags_for_post(post):
        return [t.tag for t in PostTags.objects.filter(post=post)]

    def get_posts(tag):
        tags = PostTags.objects.filter(tag=tag).order_by('-post__date')
        return [Post.process_post(tag.post) for tag in tags]


class Visits(models.Model):
    ip = models.GenericIPAddressField()
    page = models.CharField(max_length=100)
    created = models.DateTimeField(default=timezone.now)
    session = models.CharField(max_length=80, null=True)
