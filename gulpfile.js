var gulp = require('gulp'),
    sass = require('gulp-sass'),
    es = require('event-stream'),
    cleanCSS = require('gulp-clean-css'),
    uglify = require('gulp-uglify'),
    gzip = require('gulp-gzip'),
    img = require('gulp-image'),
    concat = require('gulp-concat');


var paths = {
    scripts: [''],
    scss: ['./res/sass/**/*.scss'],
    css: ['./res/sass/**/*.css']
}

gulp.task('hjs-themes', function () {
    return gulp.src('./res/vendor/highlight/styles/*.css')
        .pipe(gulp.dest('./static/lib/highlight.js/styles'));
});

gulp.task('js', function() {

    var files = ['./node_modules/jquery/dist/jquery.js',
                 './node_modules/bootstrap/dist/js/bootstrap.js',
                 './res/vendor/highlight/highlight.pack.js',
                 './res/vendor/jasny-bootstrap/js/jasny-bootstrap.js']
    gulp.src(files)
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./static/js/'));
    return gulp.src(files)
        .pipe(concat('script.js'))
        .pipe(uglify())
        .pipe(gzip())
        .pipe(gulp.dest('./static/js/'));
});

gulp.task('css', function() {
    var vendor = gulp
        .src(['./node_modules/bootstrap/dist/css/bootstrap.css',
              './node_modules/bootstrap/dist/css/bootstrap-theme.css',
              './res/vendor/jasny-bootstrap/css/jasny-bootstrap.css',
              './res/sass/my-github-md.css'])
        .pipe(cleanCSS());

    var mine = gulp.src(paths.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS());

    es.concat(vendor, mine)
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./static/css/')) ;
    return es.concat(vendor, mine)
        .pipe(concat('style.css'))
        .pipe(gzip())
        .pipe(gulp.dest('./static/css/')) ;
});

gulp.task('fonts', function() {
    return gulp.src('./node_modules/bootstrap/dist/fonts/*')
        .pipe(gulp.dest('./static/fonts/'));
});

gulp.task('img', function() {
    gulp.src('./res/img/**/*.png')
        .pipe(img())
        .pipe(gulp.dest('./static/img/'));
});

gulp.task('watch', function() {
    gulp.watch([paths.scss, paths.css], ['css']);
});

gulp.task('md', ['md-gh', 'md-hl-js', 'md-hl-css']);
gulp.task('default', ['css']);
