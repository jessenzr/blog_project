title: Welcome

# Welcome to my blog

### About me
I'm Jesse Nazario, I live in Santa Catarina, Brazil. I currently work
as a fullstack developer. As you probably have already noticed, I'm not an
English native speaker, so please correct me if you spot any error.

### About this blog
This is my personal blog where I'll be sharing things that I have
interest on, but most related to programming, probably.

### Contact
You can reach me on:

- [jessenzr@gmail.com](mailto:jessenzr@gmail.com)
- [Facebook](https://facebook.com/jessenzr){:target="_blank"}
- [Github](https://github.com/sollidsnake){:target="_blank"}

#### Credits
<small>

A big thanks to the people the tecnologies that were used to build
this website. Some them:

- [Python Flask](http://flask.pocoo.org){: target="_blank" }
- [Emacs](http://https://www.gnu.org/software/emacs/){: target="_blank" }
- [Twitter Bootstrap](http://getbootstrap.com){: target="_blank" }
- [Glyphicons](http://glyphicons.com){: target="_blank" }

</small>
