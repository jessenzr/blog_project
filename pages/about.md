title: About
meta_desc: I'm Jesse Nazario, I live in Santa Catarina, Brazil. I currently work as a fullstack developer and study Computer Science. This is my personal blog where I'll be sharing things that I have interest on, but most related to programming and computer technology.

# Welcome to my blog

### About me
I'm Jesse Nazario, I live in Santa Catarina, Brazil. I currently work
as a fullstack developer and study Computer Science. As you probably
have already noticed, I'm not an English native speaker, so please
correct me if you spot any error.

I also work as a freelancer -- if you wish to hire me, check the
contact session

### About this blog
This is my personal blog where I'll be sharing things that I have
interest on, but most related to programming and computer
technology.

### Contact<a name="contact"></a>
You can reach me on:

- [jessenzr@gmail.com](mailto:jessenzr@gmail.com)
- [Facebook](https://facebook.com/jessenzr){:target="_blank"}
- [Github](https://github.com/sollidsnake)
